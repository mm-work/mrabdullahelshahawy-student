import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mrabdullahelshahawy_student/pages/splash.dart';

import 'core/constants/theme_colors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Mr.Abdullah Elshahawy',
      home: const SplashScreen(),
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return Directionality(
          textDirection: TextDirection.rtl,
          child: child ?? const SizedBox(),
        );
      },
      theme: ThemeData(
        useMaterial3: true,
        fontFamily: GoogleFonts.markaziText().fontFamily,
        textTheme: GoogleFonts.markaziTextTextTheme(),
        colorScheme: ColorScheme.fromSeed(
          seedColor: ThemeColors.primary,
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: ButtonStyle(
            elevation: MaterialStateProperty.all(0),
            fixedSize: MaterialStateProperty.all(const Size(310, 50)),
            backgroundColor: MaterialStateProperty.all(const Color(0xFFF5F3FF)),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            elevation: MaterialStateProperty.all(0),
            textStyle: MaterialStateProperty.all(const TextStyle(
              fontSize: 16,
            )),
            fixedSize: MaterialStateProperty.all(const Size(310, 50)),
            foregroundColor: MaterialStateProperty.all(Colors.white),
            backgroundColor: MaterialStateProperty.all(ThemeColors.secondary),
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(11),
            borderSide: BorderSide(
              width: .8,
              color: ThemeColors.primary.withOpacity(.81),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(11),
            borderSide: const BorderSide(
              width: .8,
              color: Colors.black12,
            ),
          ),
        ),
      ),
    );
  }
}
