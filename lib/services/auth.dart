import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/core/api/main.dart';
import 'package:mrabdullahelshahawy_student/core/helpers/storage.dart';

class AuthService extends GetxService {
  static AuthService get to => Get.find();
  Future<AuthService> init() async {
    await getToken();
    await getUserData();
    return this;
  }

  static bool get loggenIn => to.token.isNotEmpty;

  final token = ''.obs;
  final currentName = ''.obs;
  final currentPhone = ''.obs;
  final currentCode = ''.obs;
  Future<void> getToken() async {
    final storage = Storage.get();
    token.value = (await storage.read(key: 'token')) ?? '';
    if (token.isNotEmpty) {
      DioRequest.setToken(token.value);
    }
  }

  Future<void> setToken(String t) async {
    final storage = Storage.get();
    await storage.write(key: 'token', value: t);
    token.value = t;
    if (token.isNotEmpty) {
      DioRequest.setToken(token.value);
    }
  }

  Future<void> removeToken() async {
    final storage = Storage.get();
    await storage.delete(key: 'token');
    DioRequest.removeToken();
  }

  Future<void> setUserDate(String name, String phone, String code) async {
    final storage = Storage.get();
    await storage.write(key: 'name', value: name);
    await storage.write(key: 'phone', value: phone);
    await storage.write(key: 'code', value: code);
    currentName.value = name;
    currentCode.value = code;
    currentPhone.value = phone;
  }

  Future<void> getUserData() async {
    final storage = Storage.get();
    currentName.value = (await storage.read(key: 'name')) ?? '';
    currentCode.value = (await storage.read(key: 'code')) ?? '';
    currentPhone.value = (await storage.read(key: 'phone')) ?? '';
  }
}
