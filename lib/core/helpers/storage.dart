import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class Storage {
  static AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );
  static FlutterSecureStorage get() => FlutterSecureStorage(aOptions: _getAndroidOptions());
}
