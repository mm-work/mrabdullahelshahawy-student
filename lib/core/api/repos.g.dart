// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'repos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) =>
    LoginResponse(
      json['message'] as String,
      json['statusCode'] as int,
      json['token'] as String?,
      _$recordConvert(
        json['student'],
        ($jsonValue) => (
          code: $jsonValue['code'] as int,
          name: $jsonValue['name'] as String,
          phone: $jsonValue['phone'] as String,
        ),
      ),
    );

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.statusCode,
      'token': instance.token,
      'message': instance.message,
      'student': {
        'code': instance.student.code,
        'name': instance.student.name,
        'phone': instance.student.phone,
      },
    };

$Rec _$recordConvert<$Rec>(
  Object? value,
  $Rec Function(Map) convert,
) =>
    convert(value as Map<String, dynamic>);

LecturesResponse _$LecturesResponseFromJson(Map<String, dynamic> json) =>
    LecturesResponse(
      json['statusCode'] as int,
      (json['courses'] as List<dynamic>)
          .map((e) => CourseItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LecturesResponseToJson(LecturesResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.statusCode,
      'courses': instance.courses,
    };

CourseItem _$CourseItemFromJson(Map<String, dynamic> json) => CourseItem(
      json['_id'] as String,
      json['image'] as String,
      json['isEnrolled'] as bool,
      json['name'] as String,
    );

Map<String, dynamic> _$CourseItemToJson(CourseItem instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'isEnrolled': instance.isEnrolled,
    };

LectureItem _$LectureItemFromJson(Map<String, dynamic> json) => LectureItem(
      (json['courses'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['attachments'] as List<dynamic>?)
          ?.map((e) => LectureAttachment.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['_id'] as String,
      json['image'] as String,
      json['name'] as String,
      (json['lectureParts'] as List<dynamic>?)
          ?.map((e) => LecturePart.fromJson(e as Map<String, dynamic>))
          .toList(),
      description: json['description'] as String? ?? '',
    );

Map<String, dynamic> _$LectureItemToJson(LectureItem instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'description': instance.description,
      'courses': instance.courses,
      'attachments': instance.attachments,
      'lectureParts': instance.lectureParts,
    };

LectureAttachment _$LectureAttachmentFromJson(Map<String, dynamic> json) =>
    LectureAttachment(
      json['_id'] as String,
      json['name'] as String,
      json['url'] as String,
    );

Map<String, dynamic> _$LectureAttachmentToJson(LectureAttachment instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'url': instance.url,
      'name': instance.name,
    };

LecturePart _$LecturePartFromJson(Map<String, dynamic> json) => LecturePart(
      json['_id'] as String,
      json['lectureEmbed'] as String,
      json['partName'] as String,
      json['type'] as int,
    );

Map<String, dynamic> _$LecturePartToJson(LecturePart instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'partName': instance.partName,
      'lectureEmbed': instance.lectureEmbed,
      'type': instance.type,
    };
