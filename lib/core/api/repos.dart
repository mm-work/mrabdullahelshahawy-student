import 'package:json_annotation/json_annotation.dart';

part 'repos.g.dart';

const jsonCamelCaseAnnotation = JsonSerializable();
const jsonAnnotation = JsonSerializable(
  fieldRename: FieldRename.snake,
);

@jsonCamelCaseAnnotation
class LoginResponse {
  final int statusCode;
  final String? token;
  final String message;
  final ({
    String name,
    String phone,
    int code,
  }) student;

  const LoginResponse(this.message, this.statusCode, this.token, this.student);

  factory LoginResponse.fromJson(Map<String, dynamic> json) => _$LoginResponseFromJson(json);
  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}

@jsonCamelCaseAnnotation
class LecturesResponse {
  final int statusCode;
  final List<CourseItem> courses;

  const LecturesResponse(this.statusCode, this.courses);

  factory LecturesResponse.fromJson(Map<String, dynamic> json) => _$LecturesResponseFromJson(json);
  Map<String, dynamic> toJson() => _$LecturesResponseToJson(this);
}

@jsonCamelCaseAnnotation
class CourseItem {
  @JsonKey(name: '_id')
  final String id;
  final String name;
  final String image;
  final bool isEnrolled;

  const CourseItem(
    this.id,
    this.image,
    this.isEnrolled,
    this.name,
  );

  factory CourseItem.fromJson(Map<String, dynamic> json) => _$CourseItemFromJson(json);
  Map<String, dynamic> toJson() => _$CourseItemToJson(this);
}

@jsonCamelCaseAnnotation
class LectureItem {
  @JsonKey(name: '_id')
  final String id;
  final String name;
  final String image;
  final String description;
  final List<String>? courses;
  final List<LectureAttachment>? attachments;
  final List<LecturePart>? lectureParts;

  const LectureItem(
    this.courses,
    this.attachments,
    this.id,
    this.image,
    this.name,
    this.lectureParts, {
    this.description = '',
  });
  factory LectureItem.fromJson(Map<String, dynamic> json) => _$LectureItemFromJson(json);
  Map<String, dynamic> toJson() => _$LectureItemToJson(this);
}

@jsonCamelCaseAnnotation
class LectureAttachment {
  @JsonKey(name: '_id')
  final String id;
  final String url;
  final String name;

  const LectureAttachment(
    this.id,
    this.name,
    this.url,
  );
  factory LectureAttachment.fromJson(Map<String, dynamic> json) =>
      _$LectureAttachmentFromJson(json);
  Map<String, dynamic> toJson() => _$LectureAttachmentToJson(this);
}

@jsonCamelCaseAnnotation
class LecturePart {
  @JsonKey(name: '_id')
  final String id;
  final String partName;
  final String lectureEmbed;
  final int type;

  const LecturePart(
    this.id,
    this.lectureEmbed,
    this.partName,
    this.type,
  );
  factory LecturePart.fromJson(Map<String, dynamic> json) => _$LecturePartFromJson(json);
  Map<String, dynamic> toJson() => _$LecturePartToJson(this);
}
