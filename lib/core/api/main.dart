import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response;
import 'package:mrabdullahelshahawy_student/core/constants/config.dart';
import 'package:mrabdullahelshahawy_student/pages/auth/login.dart';

import 'repos.dart';

typedef JsonData = Map<String, dynamic>;
typedef JsonResponse = Response<JsonData>;

class DioRequest {
  static Dio dio = Dio(BaseOptions(
    baseUrl: BASE_URL_API,
  ));

  static setToken(String token) {
    dio.options.headers['token'] = token;
  }

  static removeToken() {
    dio.options.headers.remove('token');
  }

  static Future<JsonResponse> post(String path, {body}) async {
    try {
      final res = await dio.post<JsonData>(path, data: body);
      if (res.data!['statusCode'] == 401 || res.statusCode == 401) {
        await Get.offAll(() => const LoginPage());
        throw Future.error('Login Need');
      }
      return res;
    } on DioException {
      Get.rawSnackbar(message: 'خطأ في السيرفر');
      rethrow;
    }
  }

  static Future<JsonResponse> get(String path) async {
    try {
      final res = await dio.get<JsonData>(path);
      if (res.data!['statusCode'] == 401 || res.statusCode == 401) {
        await Get.offAll(() => const LoginPage());
        throw Future.error('Login Need');
      }
      return res;
    } on DioException {
      Get.rawSnackbar(message: 'خطأ في السيرفر');
      rethrow;
    }
  }

  static Future<Response> getNative(String path) async {
    try {
      final res = await dio.get(path);
      if (res.data!['statusCode'] == 401 || res.statusCode == 401) {
        await Get.offAll(() => const LoginPage());
        throw Future.error('Login Need');
      }
      return res;
    } on DioException {
      Get.rawSnackbar(message: 'خطأ في السيرفر');
      rethrow;
    }
  }
}

class AuthAPI {
  static Future<LoginResponse> login(String username, String password) async {
    final res = await DioRequest.post('/login', body: {
      'username': username,
      'password': password,
    });
    return LoginResponse.fromJson(res.data!);
  }

  static Future<LoginResponse> register(
    String name,
    String username,
    String password,
    String parentPhone,
    int stage,
  ) async {
    final res = await DioRequest.post('/register', body: {
      'student_name': name,
      'username': username,
      'pass': password,
      'parent1_phone': username,
      'parent2_phone': parentPhone,
      'stage': stage,
    });
    return LoginResponse.fromJson(res.data!);
  }
}

class LecturesAPI {
  static Future<LecturesResponse> lectures() async {
    final res = await DioRequest.get('/lectures');
    return LecturesResponse.fromJson(res.data!);
  }

  static Future<List<LectureItem>> get(String id) async {
    final res = await DioRequest.get('/courses/$id/');
    return (res.data!['lectures'] as List).map((e) => LectureItem.fromJson(e)).toList();
  }
}

class CodesAPI {
  static Future buyWithCodes(String code) async {
    final res = await DioRequest.post('/lectures/buyWithCodes', body: {
      'code': code,
    });
    return (res.data!['lectures'] as List).map((e) => LectureItem.fromJson(e)).toList();
  }
}
