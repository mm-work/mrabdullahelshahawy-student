import 'package:flutter/material.dart';

class ThemeColors {
  static const Color primary = Color(0xFF673974);
  static const Color secondary = Color(0xFFCEA461);
}
