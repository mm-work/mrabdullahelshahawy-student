import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/core/api/main.dart';
import 'package:mrabdullahelshahawy_student/pages/home.dart';
import 'package:mrabdullahelshahawy_student/services/auth.dart';

import 'register.dart';

class LoginController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final phoneNum = TextEditingController();
  final password = TextEditingController();

  Future<void> submit() async {
    if (formKey.currentState!.validate() == false) return;

    final res = await AuthAPI.login(phoneNum.text, password.text);

    if (res.statusCode == 200) {
      await AuthService.to.setToken(res.token!);
      await AuthService.to.setUserDate(
        res.student.name,
        res.student.phone,
        res.student.code.toString(),
      );
      Get.rawSnackbar(message: res.message);
      Get.offAll(() => const HomePage());
      return;
    }
    Get.rawSnackbar(message: res.message);
  }
}

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  static final bgObjects = [
    Positioned(
      right: -100,
      width: 350,
      height: 200,
      bottom: -40,
      child: Transform.rotate(
        angle: 1,
        child: const ClipOval(
          child: Material(
            color: Color(0xFF72477E),
          ),
        ),
      ),
    ),
    Positioned(
      left: -100,
      width: 350,
      height: 200,
      top: -50,
      child: Transform.rotate(
        angle: 1,
        child: const ClipOval(
          child: Material(
            color: Color(0xFF72477E),
          ),
        ),
      ),
    ),
    const Positioned(
      right: -100,
      width: 200,
      height: 200,
      top: -50,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: -140,
      width: 200,
      height: 200,
      bottom: -70,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: 70,
      width: 100,
      height: 100,
      bottom: 300,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: 40,
      width: 100,
      height: 100,
      bottom: 150,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      right: 40,
      width: 100,
      height: 100,
      top: 150,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      right: 90,
      width: 100,
      height: 100,
      top: 300,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: 30,
      width: 100,
      height: 100,
      top: 290,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      right: 30,
      width: 100,
      height: 100,
      bottom: 290,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final c = Get.put(LoginController());
    return Stack(
      children: [
        Positioned.fill(
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                stops: [0, .3],
                colors: [
                  Color.fromARGB(255, 104, 48, 119),
                  Color(0xFF673974),
                ],
              ),
            ),
          ),
        ),
        ...bgObjects,
        Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/logo.png',
                    width: 280,
                  ),
                  const SizedBox(height: 26),
                  Container(
                    width: 300,
                    padding: const EdgeInsets.only(
                      top: 26,
                      bottom: 15,
                      left: 16,
                      right: 16,
                    ),
                    decoration: BoxDecoration(
                      color: const Color(0xFFFDF6FF),
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: const [
                        BoxShadow(
                          blurRadius: 20,
                          spreadRadius: 9,
                          color: Colors.black26,
                        )
                      ],
                    ),
                    child: Form(
                      key: c.formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'اهلا بالطالب',
                            style: theme.textTheme.displayMedium?.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            'برجاء تسجيل الدخول',
                            style: theme.textTheme.labelLarge,
                          ),
                          const SizedBox(height: 30),
                          TextFormField(
                            controller: c.phoneNum,
                            textInputAction: TextInputAction.next,
                            decoration: const InputDecoration(
                              labelText: 'رقم الهاتف',
                            ),
                            validator: (value) {
                              if (value != null || value!.isEmpty) return null;
                              return 'يرجى ملئ البيانات بشكل صحيح';
                            },
                          ),
                          const SizedBox(height: 14),
                          TextFormField(
                            obscureText: true,
                            controller: c.password,
                            textInputAction: TextInputAction.next,
                            decoration: const InputDecoration(
                              labelText: 'كلمة المرور',
                            ),
                            validator: (value) {
                              if (value != null || value!.isEmpty) return null;
                              return 'يرجى ملئ البيانات بشكل صحيح';
                            },
                          ),
                          const SizedBox(height: 16),
                          ElevatedButton(
                            onPressed: c.submit,
                            child: const Text('تسجيل الدخول'),
                          ),
                          const SizedBox(height: 11),
                          TextButton(
                            onPressed: () {
                              Get.to(() => const RegisterPage());
                            },
                            child: const Text('حساب جديد'),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 80),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
