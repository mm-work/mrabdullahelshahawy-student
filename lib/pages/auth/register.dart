import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/core/api/main.dart';
import 'package:mrabdullahelshahawy_student/pages/home.dart';
import 'package:mrabdullahelshahawy_student/services/auth.dart';

class RegisterController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final name = TextEditingController();
  final phoneNum = TextEditingController();
  final parentPhoneNum = TextEditingController();
  final password = TextEditingController();
  final stage = RxnInt();

  Future<void> submit() async {
    if (formKey.currentState!.validate() == false) return;

    final res = await AuthAPI.register(
      name.text,
      phoneNum.text,
      password.text,
      parentPhoneNum.text,
      stage.value!,
    );

    if (res.statusCode == 200) {
      await AuthService.to.setToken(res.token!);
      Get.rawSnackbar(message: res.message);
      Get.offAll(() => const HomePage());
      return;
    }
    Get.rawSnackbar(message: res.message);
  }
}

class RegisterPage extends StatelessWidget {
  const RegisterPage({super.key});

  static final bgObjects = [
    Positioned(
      right: -100,
      width: 350,
      height: 200,
      bottom: -40,
      child: Transform.rotate(
        angle: 1,
        child: const ClipOval(
          child: Material(
            color: Color(0xFF72477E),
          ),
        ),
      ),
    ),
    Positioned(
      left: -100,
      width: 350,
      height: 200,
      top: -50,
      child: Transform.rotate(
        angle: 1,
        child: const ClipOval(
          child: Material(
            color: Color(0xFF72477E),
          ),
        ),
      ),
    ),
    const Positioned(
      right: -100,
      width: 200,
      height: 200,
      top: -50,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: -140,
      width: 200,
      height: 200,
      bottom: -70,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: 70,
      width: 100,
      height: 100,
      bottom: 300,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: 40,
      width: 100,
      height: 100,
      bottom: 150,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      right: 40,
      width: 100,
      height: 100,
      top: 150,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      right: 90,
      width: 100,
      height: 100,
      top: 300,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      left: 30,
      width: 100,
      height: 100,
      top: 290,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
    const Positioned(
      right: 30,
      width: 100,
      height: 100,
      bottom: 290,
      child: ClipOval(
        child: Material(
          color: Color(0xFF72477E),
        ),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final c = Get.put(RegisterController());
    return Stack(
      children: [
        Positioned.fill(
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                stops: [0, .3],
                colors: [
                  Color.fromARGB(255, 104, 48, 119),
                  Color(0xFF673974),
                ],
              ),
            ),
          ),
        ),
        ...bgObjects,
        Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/logo.png',
                    width: 280,
                  ),
                  const SizedBox(height: 26),
                  Container(
                    width: 300,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 26,
                    ),
                    decoration: BoxDecoration(
                      color: const Color(0xFFFDF6FF),
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: const [
                        BoxShadow(
                          blurRadius: 20,
                          spreadRadius: 9,
                          color: Colors.black26,
                        )
                      ],
                    ),
                    child: Form(
                      key: c.formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'اهلا بالطالب',
                            style: theme.textTheme.displayMedium?.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            'برجاء تسجيل الدخول',
                            style: theme.textTheme.labelLarge,
                          ),
                          const SizedBox(height: 30),
                          TextFormField(
                            controller: c.name,
                            textInputAction: TextInputAction.next,
                            decoration: const InputDecoration(
                              labelText: 'الأسم',
                            ),
                            validator: (value) {
                              if (value != null || value!.isEmpty) return null;
                              return 'يرجى ملئ البيانات بشكل صحيح';
                            },
                          ),
                          const SizedBox(height: 14),
                          TextFormField(
                            controller: c.phoneNum,
                            textInputAction: TextInputAction.next,
                            decoration: const InputDecoration(
                              labelText: 'رقم الهاتف',
                            ),
                            validator: (value) {
                              if (value != null || value!.isEmpty) {
                                if (value.startsWith('0') != true) {
                                  return 'يجب ان تبدأ ب 0';
                                }
                                if (value.length <= 10) {
                                  return 'يجب ان لا يقل عن 11';
                                }
                                return null;
                              }
                              return 'يرجى ملئ البيانات بشكل صحيح';
                            },
                          ),
                          const SizedBox(height: 14),
                          TextFormField(
                            controller: c.parentPhoneNum,
                            textInputAction: TextInputAction.next,
                            decoration: const InputDecoration(
                              labelText: 'رقم الهاتف الوالد',
                            ),
                            validator: (value) {
                              if (value != null || value!.isEmpty) return null;
                              return 'يرجى ملئ البيانات بشكل صحيح';
                            },
                          ),
                          const SizedBox(height: 14),
                          TextFormField(
                            obscureText: true,
                            controller: c.password,
                            textInputAction: TextInputAction.next,
                            decoration: const InputDecoration(
                              labelText: 'كلمة المرور',
                            ),
                            validator: (value) {
                              if (value != null || value!.isEmpty) return null;
                              return 'يرجى ملئ البيانات بشكل صحيح';
                            },
                          ),
                          const SizedBox(height: 14),
                          TextFormField(
                            obscureText: true,
                            textInputAction: TextInputAction.next,
                            decoration: const InputDecoration(
                              labelText: 'إعادة كلمة المرور كلمة المرور',
                            ),
                            validator: (value) {
                              if (value != null || value!.isEmpty) {
                                if (value != c.password.text) {
                                  return 'كلمة المرور غير متطابقة';
                                }
                                return null;
                              }
                              return 'يرجى ملئ البيانات بشكل صحيح';
                            },
                          ),
                          const SizedBox(height: 14),
                          Obx(() => DropdownButtonFormField<int>(
                                value: c.stage.value,
                                onChanged: c.stage,
                                decoration: const InputDecoration(
                                  labelText: 'المرحلة',
                                ),
                                items: const [
                                  DropdownMenuItem(
                                    value: 2,
                                    child: Text('تانية ثانوي'),
                                  ),
                                  DropdownMenuItem(
                                    value: 3,
                                    child: Text('تالتة ثانوي'),
                                  ),
                                ],
                              )),
                          const SizedBox(height: 16),
                          ElevatedButton(
                            onPressed: c.submit,
                            child: const Text('تسجيل الدخول'),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 80),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
