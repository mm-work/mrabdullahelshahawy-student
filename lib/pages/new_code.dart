import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/core/api/main.dart';
import 'package:mrabdullahelshahawy_student/core/api/repos.dart';
import 'package:mrabdullahelshahawy_student/core/constants/config.dart';

import 'lectures/lecture_view.dart';

class NewCodeInputDialog extends StatelessWidget {
  const NewCodeInputDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final code = ''.obs;
    return Dialog(
      child: Padding(
        padding: const EdgeInsets.all(11),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('اكتب الكود الجديد'),
                  Divider(),
                ],
              ),
            ),
            TextField(
              onChanged: code,
              decoration: const InputDecoration(
                labelText: 'الكود الجديد',
              ),
            ),
            const SizedBox(height: 11),
            ElevatedButton(
              child: const Text('تسجيل'),
              onPressed: () async {
                final lecs = await CodesAPI.buyWithCodes(code.value);
                Get.to(() => ValidLecToBuy(lecs));
              },
            ),
          ],
        ),
      ),
    );
  }
}

class ValidLecToBuy extends StatelessWidget {
  const ValidLecToBuy(this.lecs, {super.key});

  final List<LectureItem> lecs;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('شراء المحاضرات'),
      ),
      body: ListView.builder(
        itemCount: lecs.length,
        itemBuilder: (context, index) {
          final lecture = lecs[index];
          return Container(
            padding: const EdgeInsets.all(21),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(22),
              boxShadow: const [
                BoxShadow(
                  blurRadius: 3,
                  spreadRadius: 1,
                  color: Colors.black12,
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(22),
                  child: Image.network(BASE_URL + lecture.image),
                ),
                const SizedBox(height: 8),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 11),
                  child: Text(
                    lecture.name,
                    style: theme.textTheme.headlineMedium?.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                const SizedBox(height: 11),
                OutlinedButton(
                  child: const Text('مشاهدة المحاضرة'),
                  onPressed: () {
                    Get.to(() => LectureViewPage(lecture));
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
