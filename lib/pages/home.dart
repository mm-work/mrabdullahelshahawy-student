import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/core/api/main.dart';
import 'package:mrabdullahelshahawy_student/core/api/repos.dart';
import 'package:mrabdullahelshahawy_student/core/constants/config.dart';
import 'package:mrabdullahelshahawy_student/pages/auth/login.dart';
import 'package:mrabdullahelshahawy_student/pages/lectures/course_view.dart';
import 'package:mrabdullahelshahawy_student/services/auth.dart';

import 'new_code.dart';

class HomeController extends GetxController {
  final courses = <CourseItem>[].obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    courses.value = (await LecturesAPI.lectures()).courses;
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final c = Get.put(HomeController());
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Color.fromARGB(255, 87, 40, 100),
      ),
      child: Scaffold(
        backgroundColor: const Color(0xFFFDF6FF),
        floatingActionButton: FloatingActionButton(
          elevation: .81,
          backgroundColor: theme.primaryColor,
          foregroundColor: theme.colorScheme.onPrimary,
          child: const Text(
            'كود\nجديد',
            textAlign: TextAlign.center,
          ),
          onPressed: () {
            Get.dialog(const NewCodeInputDialog());
          },
        ),
        body: ListView(
          children: [
            Container(
              height: 100,
              clipBehavior: Clip.antiAlias,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(27),
                ),
                gradient: LinearGradient(
                  stops: [0, .3],
                  colors: [
                    Color.fromARGB(255, 104, 48, 119),
                    Color(0xFF673974),
                  ],
                ),
              ),
              child: Stack(
                children: [
                  Positioned(
                    right: -10,
                    width: 220,
                    height: 100,
                    bottom: -20,
                    child: Transform.rotate(
                      angle: .35,
                      child: const ClipOval(
                        child: Material(
                          color: Color(0xFF72477E),
                        ),
                      ),
                    ),
                  ),
                  Positioned.fill(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 14,
                        vertical: 21,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AuthService.to.currentName.value,
                                  overflow: TextOverflow.fade,
                                  style: theme.textTheme.headlineMedium?.copyWith(
                                    color: Colors.white,
                                  ),
                                ),
                                Text(
                                  'رقم الهاتف: ${AuthService.to.currentPhone}',
                                  overflow: TextOverflow.fade,
                                  style: theme.textTheme.labelLarge?.copyWith(
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              await AuthService.to.removeToken();
                              Get.offAll(() => const LoginPage());
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  '#${AuthService.to.currentCode.value}',
                                  overflow: TextOverflow.fade,
                                  style: theme.textTheme.headlineMedium?.copyWith(
                                    letterSpacing: 1.12,
                                    fontWeight: FontWeight.w600,
                                    color: const Color(0xFFFFD694),
                                  ),
                                ),
                                Text(
                                  'تسجيل خروج',
                                  overflow: TextOverflow.fade,
                                  style: theme.textTheme.labelLarge?.copyWith(
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 21),
            Obx(() => ListView.separated(
                  shrinkWrap: true,
                  itemCount: c.courses.length,
                  padding: const EdgeInsets.all(11),
                  physics: const NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) {
                    return const SizedBox(height: 16);
                  },
                  itemBuilder: (context, index) {
                    final course = c.courses[index];
                    return Container(
                      padding: const EdgeInsets.all(21),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(22),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(22),
                            child: Image.network(BASE_URL + course.image),
                          ),
                          const SizedBox(height: 8),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 11),
                            child: Text(
                              course.name,
                              style: theme.textTheme.headlineMedium?.copyWith(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          const SizedBox(height: 11),
                          if (course.isEnrolled)
                            OutlinedButton(
                              child: const Text('مشاهدة المحاضرات'),
                              onPressed: () {
                                Get.to(() => CourseViewPage(course));
                              },
                            )
                          else
                            ElevatedButton(
                              child: const Text('الاشتراك'),
                              onPressed: () {},
                            ),
                        ],
                      ),
                    );
                  },
                ))
          ],
        ),
      ),
    );
  }
}
