import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/pages/home.dart';
import 'package:mrabdullahelshahawy_student/services/auth.dart';

import 'auth/login.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    setup();
  }

  void setup() async {
    await Get.putAsync(() => AuthService().init());
    await Future.delayed(const Duration(seconds: 2));

    if (AuthService.loggenIn == false) {
      Get.offAll(() => const LoginPage());
      return;
    }
    Get.offAll(() => const HomePage());
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Hero(
              tag: 'imageHero',
              child: Image(
                width: 275,
                image: AssetImage('assets/logo.png'),
              ),
            ),
            SizedBox(height: 21),
            SizedBox(
              width: 200,
              child: LinearProgressIndicator(),
            ),
            SizedBox(height: 51),
          ],
        ),
      ),
    );
  }
}
