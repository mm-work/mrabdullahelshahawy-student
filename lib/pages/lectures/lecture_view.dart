import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/core/api/repos.dart';
import 'package:mrabdullahelshahawy_student/core/constants/config.dart';
import 'package:mrabdullahelshahawy_student/core/constants/theme_colors.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:mrabdullahelshahawy_student/services/auth.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class LectureViewPage extends StatelessWidget {
  const LectureViewPage(this.lectureItem, {super.key});

  final LectureItem lectureItem;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(lectureItem.name),
      ),
      body: ListView(
        padding: const EdgeInsets.all(11),
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(13),
            child: Image.network(BASE_URL + lectureItem.image),
          ),
          const SizedBox(height: 11),
          Container(
            padding: const EdgeInsets.all(11),
            decoration: BoxDecoration(
              color: const Color(0xFFf5f3ff),
              borderRadius: BorderRadius.circular(6),
              border: Border.all(
                color: ThemeColors.primary,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.info_outline,
                      color: ThemeColors.secondary,
                    ),
                    const SizedBox(width: 7),
                    Text(
                      'وصف المحاضرة',
                      style: theme.textTheme.headlineMedium?.copyWith(
                        fontWeight: FontWeight.w600,
                        color: ThemeColors.secondary,
                      ),
                    ),
                  ],
                ),
                Html(
                  data: lectureItem.description,
                ),
              ],
            ),
          ),
          const SizedBox(height: 16),
          Container(
            padding: const EdgeInsets.all(11),
            decoration: BoxDecoration(
              color: const Color(0xFFf5f3ff),
              borderRadius: BorderRadius.circular(6),
              border: Border.all(
                color: ThemeColors.primary,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.file_present,
                      color: ThemeColors.secondary,
                    ),
                    const SizedBox(width: 7),
                    Text(
                      'الملفات',
                      style: theme.textTheme.headlineMedium?.copyWith(
                        fontWeight: FontWeight.w600,
                        color: ThemeColors.secondary,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 7),
                for (final file in lectureItem.attachments!)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    child: Material(
                      color: Colors.white,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(11),
                      child: ListTile(
                        // leading: const Icon(Icons.file_present_outlined),
                        trailing: const Icon(Icons.open_in_browser),
                        title: Text(file.name),
                        onTap: () {
                          launchUrl(Uri.parse(BASE_URL + file.url));
                        },
                      ),
                    ),
                  ),
              ],
            ),
          ),
          const SizedBox(height: 16),
          Container(
            padding: const EdgeInsets.all(11),
            decoration: BoxDecoration(
              color: const Color(0xFFf5f3ff),
              borderRadius: BorderRadius.circular(6),
              border: Border.all(
                color: ThemeColors.primary,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.view_agenda_rounded,
                      color: ThemeColors.secondary,
                    ),
                    const SizedBox(width: 7),
                    Text(
                      'أجزاء المحاضرة',
                      style: theme.textTheme.headlineMedium?.copyWith(
                        fontWeight: FontWeight.w600,
                        color: ThemeColors.secondary,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 7),
                for (final part in lectureItem.lectureParts!)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    child: Material(
                      color: Colors.white,
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(11),
                      child: ListTile(
                        // leading: const Icon(Icons.file_present_outlined),
                        trailing: const Icon(Icons.open_in_new),
                        title: Text(part.partName),
                        onTap: () {
                          Get.to(() => _PartView(
                                lecId: lectureItem.id,
                                partName: part.partName,
                                partIndex: lectureItem.lectureParts!.indexOf(part),
                              ));
                        },
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _PartView extends StatefulWidget {
  const _PartView({
    // super.key,
    required this.lecId,
    required this.partIndex,
    required this.partName,
  });

  final int partIndex;
  final String lecId;
  final String partName;

  @override
  State<_PartView> createState() => _PartViewState();
}

class _PartViewState extends State<_PartView> {
  late final WebViewController controller;

  @override
  void initState() {
    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
        ),
      )
      ..loadRequest(Uri.parse('$BASE_URL_API/lectures/${widget.lecId}/watch/${widget.partIndex}'),
          headers: {
            'token': AuthService.to.token.value,
          });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.partName),
      ),
      body: WebViewWidget(controller: controller),
    );
  }
}
