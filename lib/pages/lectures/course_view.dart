import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mrabdullahelshahawy_student/core/api/main.dart';
import 'package:mrabdullahelshahawy_student/core/api/repos.dart';
import 'package:mrabdullahelshahawy_student/core/constants/config.dart';
import 'package:mrabdullahelshahawy_student/pages/lectures/lecture_view.dart';

class CourseViewController extends GetxController {
  final String id;
  CourseViewController(this.id);

  final lectures = <LectureItem>[].obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    lectures.value = await LecturesAPI.get(id);
  }
}

class CourseViewPage extends StatelessWidget {
  const CourseViewPage(this.lecItem, {super.key});

  final CourseItem lecItem;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final c = Get.put(CourseViewController(lecItem.id));
    return Scaffold(
      appBar: AppBar(
        title: Text(lecItem.name),
      ),
      body: ListView(
        children: [
          Obx(() => ListView.separated(
                shrinkWrap: true,
                itemCount: c.lectures.length,
                padding: const EdgeInsets.all(11),
                physics: const NeverScrollableScrollPhysics(),
                separatorBuilder: (context, index) {
                  return const SizedBox(height: 21);
                },
                itemBuilder: (context, index) {
                  final lecture = c.lectures[index];
                  return Container(
                    padding: const EdgeInsets.all(21),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(22),
                      boxShadow: const [
                        BoxShadow(
                          blurRadius: 3,
                          spreadRadius: 1,
                          color: Colors.black12,
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(22),
                          child: Image.network(BASE_URL + lecture.image),
                        ),
                        const SizedBox(height: 8),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 11),
                          child: Text(
                            lecture.name,
                            style: theme.textTheme.headlineMedium?.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        const SizedBox(height: 11),
                        OutlinedButton(
                          child: const Text('مشاهدة المحاضرة'),
                          onPressed: () {
                            Get.to(() => LectureViewPage(lecture));
                          },
                        ),
                      ],
                    ),
                  );
                },
              )),
        ],
      ),
    );
  }
}
